import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutoServiceRoutingModule } from './auto-service-routing.module';
import { NavComponent } from './nav/nav.component';
import { ServiceScrollerComponent } from './service-scroller/service-scroller.component';
import { SearchCarComponent } from './search-car/search-car.component';
import { ServiceDetailsComponent } from './service-details/service-details.component';
import { ServiceLocationComponent } from './service-location/service-location.component';
import { ServiceNavComponent } from './service-nav/service-nav.component';
import { ServiceFooterComponent } from './service-footer/service-footer.component';


@NgModule({
  declarations: [
    NavComponent,
    ServiceScrollerComponent,
    SearchCarComponent,
    ServiceDetailsComponent,
    ServiceLocationComponent,
    ServiceNavComponent,
    ServiceFooterComponent
  ],
  imports: [
    CommonModule,
    AutoServiceRoutingModule
  ]
})
export class AutoServiceModule { }
