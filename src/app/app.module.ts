import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { FloatingBarComponent } from './floating-bar/floating-bar.component';


import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field' ;
import {MatGridListModule} from '@angular/material/grid-list';


import { QuickBookComponent } from './quick-book/quick-book.component';
import { OffersComponent } from './offers/offers.component';
import { OurServicesComponent } from './our-services/our-services.component';
import { WhatWhyComponent } from './what-why/what-why.component';
import { HowComponent } from './how/how.component';
import { ClientsComponent } from './clients/clients.component';
import { PressComponent } from './press/press.component';
import { ReviewComponent } from './review/review.component';
import { HomeComponent } from './home/home.component';




@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    FloatingBarComponent,
    QuickBookComponent,
    OffersComponent,
    OurServicesComponent,
    WhatWhyComponent,
    HowComponent,
    ClientsComponent,
    PressComponent,
    ReviewComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatGridListModule
  ],
  providers: [{provide: Window, useValue: window}],
  bootstrap: [AppComponent]
})
export class AppModule { }
